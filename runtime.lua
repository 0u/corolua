local runtime = {}

-- a future can be polled until done.
-- it returns nil until done, where it returns a value.
-- type future = fn() -> !nil,

function runtime.sleep(n)
  local start = os.time()
  return coroutine.yield(function()
    if os.time() >= start + n then
      return 'sleep ' .. tostring(n) .. ' complete!'
    end
  end)
end

function runtime.newFuture(coro)
  local poll
  local argsForCoro = {}

  return {
    poll = function()
      if poll then
        local results = { poll() }
        if results[1] ~= nil then
          argsForCoro = results
          poll = nil
        end
        return nil
      end

      local args = { coroutine.resume(coro, table.unpack(argsForCoro)) }
      if not args[1] then error(args[2]) end
      if type(args[2]) == 'function' then
        poll = args[2]
      elseif args[2] ~= nil then
        error('only valid yield is function, got ' .. type(args[2]) .. ' val: ' .. tostring(args[2]))
      else -- coro ended
        return true
      end
    end,
  }
end

function runtime.run(funcs)
  -- convert funcs to coroutines
  local futures = {}
  for _, func in ipairs(funcs) do
    table.insert(futures, runtime.newFuture(coroutine.create(func)))
  end

  -- loop through futures over and over until there are no more futures left.

  local i = 0
  while true do
    local future = futures[i+1]
    if future.poll() then
      table.remove(futures, i + 1)
      if #futures == 0 then break end
    end
    i = (i + 1) % #futures
  end
end

return runtime
