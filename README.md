## corolua

Simple scheduler for coroutine tasks in lua, currently only with the `sleep()` future.

futures are just a poll function, and can be implemented like this:
```lua
function runtime.sleep(n)
  -- setup poll function state here, as local variables
  local start = os.time()

  -- yield our poll function to the scheduler
  return coroutine.yield(function()
    if os.time() >= start + n then
      -- when poll is done return a non-nil value,
      -- which will be passed back to the calling coroutine.
      return true
    end
  end)
end
```

you can run functions concurrently like this
```lua
local runtime = require('runtime')

function coro1()
  print('[c1] Sleeping for 1 seconds...')
  runtime.sleep(1) -- no await needed, I love lua :D
  print('[c1] Done!')
end

function coro2()
  print('[c2] Sleeping for 2 seconds...')
  runtime.sleep(2)
  print('[c2] Done!')
end

runtime.run({coro1, coro2})
```
