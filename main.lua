local runtime = require('runtime')


function coro1()
  print('[c1] Sleeping for 1 seconds...')
  print(runtime.sleep(1))
  print('[c1] Done!')
end

function coro2()
  print('[c2] Sleeping for 2 seconds...')
  print(runtime.sleep(2))
  print('[c2] Done!')
end

runtime.run({coro1, coro2})
